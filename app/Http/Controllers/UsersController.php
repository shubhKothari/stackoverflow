<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;

class UsersController extends Controller
{
    

    public function notifications(){
        //auth()->readnotifications
    //auth()->user
    auth()->user()->unreadNotifications->markAsRead();

    $notifications = auth()->user()->notifications()->paginate(10);
    return view('users.notifications', compact([
        'notifications'
    ]));
    }
}
