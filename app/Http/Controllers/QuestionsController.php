<?php

namespace App\Http\Controllers;

/**
 * I made a mistake in making request (command) i couldnt debug it (i tried to make changes in autoload file bt i couldnt so for now i have made another CreateQuestionsRequest (Question(s) - 's' is the chnage)
 * 
 * I made a mistake in AnswerPoilcy spelling mistake AnswerPolicy** (i have kept it as it is for know)
 */

// use App\Http\Requests\CreateQuestionRequest as RequestsCreateQuestionRequest;

// use App\Http\Requests\CreateQuestionRequest as RequestsCreateQuestionRequest;
// use App\Http\Requests\Questions\CreateQuestionRequest;
use App\Http\Requests\Questions\CreateQuestionsRequest;
use App\Http\Requests\Questions\UpdateQuestionRequest;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class QuestionsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth'])->only(['create', 'store', 'edit', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::with('owner')->latest()->paginate(10); //eager load
        return view('questions.index', compact([
            'questions'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        app('debugbar')->disable();
        return view('questions.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateQuestionsRequest $request)
    {
        auth()->user()->questions()->create([
            'title' => $request->title,
            'body' => $request->body
        ]);

        session()->flash('success', "Question has been added successfully!");
        return redirect(route('questions.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        $question->increment('views_count');
        return view('questions.show', compact([
            'question'
        ]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        // if(Gate::allows('update-question', $question)){
        //     app('debugbar')->disable();
        //     return view('questions.edit', compact([
        //         'question'
        //     ]));
        // }
        if($this->authorize('update', $question)){
            app('debugbar')->disable();
            return view('questions.edit', compact([
                'question'
            ]));
        }
        abort(403);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateQuestionRequest $request, Question $question)
    {
    //     if(Gate::allows('update-question', $question)){
    //         $question->update([
    //             'title' => $request->title,
    //             'body' => $request->body
    //         ]);
    
    //         session()->flash('success', "Question has been updated successfully!");
    //         return redirect(route('questions.index'));
    //     }
        if($this->authorize('update', $question)){
            $question->update([
                'title' => $request->title,
                'body' => $request->body
            ]);
    
            session()->flash('success', "Question has been updated successfully!");
            return redirect(route('questions.index'));
        }
        abort(403);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {
        // if(auth()->user()->can('delete-question', $question)){
        //     $question->delete();
        //     session()->flash('success', "Question has been deleted successfully!");
        //     return redirect(route('questions.index'));
        // }
        if($this->authorize('delete', $question)){
            $question->delete();
            session()->flash('success', "Question has been deleted successfully!");
            return redirect(route('questions.index'));
        }
        abort(403);
    }
}
